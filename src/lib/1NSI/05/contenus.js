import { readable } from 'svelte/store';

export const contenus = readable(
    [{
        href:'/1NSI/05/01-bulles_par_deux', texte:'Bulles deux par deux'
    },
    {
        href:'/1NSI/05/02-binaire_vers_decimal', texte:'Du binaire au décimal'
    }
    ]
);