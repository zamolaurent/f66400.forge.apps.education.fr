import { quintOut } from 'svelte/easing';
import { crossfade } from 'svelte/transition';
import { flip } from 'svelte/animate';

export { quintOut } ;
export { crossfade } ;
export { flip } ;

import { writable } from 'svelte/store';
export const animer = writable(false);

export const [send, receive] = crossfade({
    fallback(node, params) {
        if(! animer){return 0}

        const style = getComputedStyle(node);
        const transform = style.transform === 'none' ? '' : style.transform;

        animer.set(false);

        return {
            duration: 600,
            easing: quintOut,
            css: (t) => `
                transform: ${transform} scale(${t});
                opacity: ${t}
            `
        };
    }
});