export const creer_file = function (){
    console.log("file créée");
    return []
}

export const  creer_pile = function(){
    console.log("pile créée");
    return []
}

export const est_vide = function( tab ){
    return tab.length === 0;
}

export const depiler = function( pile ){
    return pile.pop()
}

export const empiler = function(P, elt){
    P. push(elt);
}

export const Piles = {creer_pile:creer_pile, 
		 est_vide:est_vide, 
		 depiler:depiler, 
		 empiler:empiler}